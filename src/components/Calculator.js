import React from "react";


export const Calculator = ({value, actions})=>
        <div className="calculator">
            <p>{value}</p>
            <button onClick={()=>actions.addOne(10)}>+1</button>
            <button onClick={actions.subtractOne}>-1</button>
            <button onClick={actions.doubleValue}>{String.fromCharCode(215)}2</button>
        </div>

