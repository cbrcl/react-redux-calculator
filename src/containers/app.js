import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as MathActions from "../actions/mathActions";
import { Calculator } from "../components/Calculator"


class App extends React.Component {
    render() {
        const { value, actions } = this.props;

        return (
            <Calculator actions={actions} value={value} />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        value: state.math.value
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(MathActions, dispatch)
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);