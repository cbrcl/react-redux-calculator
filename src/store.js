import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import math from "./reducers/mathReducer";
import user from "./reducers/userReducer";

const store = createStore(
    combineReducers({math, user}),
    {},
    applyMiddleware(logger, thunk)
);

store.subscribe(()=>{
    console.log(store.getState());
});

export default store;
