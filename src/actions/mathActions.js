import * as types from "../constants/actionTypes";

export function addOne(value) {
    return {
        type: types.ADD_ONE,
        payload: value
    };
}

export function subtractOne() {
    return {
        type: types.SUBTRACT_ONE,
        payload: 20
    };
}

export function doubleValue() {

    return dispatch => {
        setTimeout(() => {
            dispatch({
                type: types.DOUBLE_VALUE,
                payload: 1000
            });
        }, 2000);
    }


}